package com.example.demo.ex4;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummyLogger2 {

    private void sayHi() {
        log.info("Hi from zadanie4");
    }
}
