package com.example.demo.ex2;

import com.example.demo.DummyLogger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CommandLineRunnerConstructorInjection implements CommandLineRunner {

    private DummyLogger dummyLogger;

    public CommandLineRunnerConstructorInjection(DummyLogger dummyLogger) {
        this.dummyLogger = dummyLogger;
    }

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger.sayHello("constructor");
    }



}
