package com.example.demo.ex3;

import org.springframework.stereotype.Component;

@Component
public class DummyLoggerImpl implements DummyLoggerInterface {
    @Override
    public void sayHello() {
        System.out.println(DummyLoggerImpl.class.getName());
    }
}
