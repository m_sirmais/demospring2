package com.example.demo;

import com.example.demo.DummyLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;

@Component
public class DummyLogger implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello ex1");
    }

    public void sayHello(String text) {
        System.out.println("hello from DummyLogger" + text);
    }

}
